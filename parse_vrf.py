#!/usr/bin/env python

import re
import json

load_from_file = open("router_test_output.txt", "r").read()

new_load = load_from_file.split("!")

vrf_name = []

for anim in new_load:
    if "vrf definition" in anim:
        vrf_name.append(anim.replace("\n", " "))
    if "ip vrf " in anim and " ip vrf " not in anim:
        vrf_name.append(anim.replace("\n", " "))


filter_vrf_name = "\s*vrf definition (.*) \s*"
filter_ip_vrf_name = "\s*ip vrf (.*) \s*"
filter_route_distinguisher = "\s* rd (.*)"

only_vrf_name =[]
get_rd = []

for anim in vrf_name:

    re_to_vrf_name = re.search(filter_vrf_name, anim)
    if re_to_vrf_name and re_to_vrf_name.group(1):
        only_vrf_name.append(re_to_vrf_name.group(1))
    re_to_ip_vrf_name = re.search(filter_ip_vrf_name, anim)
    if re_to_ip_vrf_name and re_to_ip_vrf_name.group(1):
        only_vrf_name.append(re_to_ip_vrf_name.group(1))
    re_to_rd = re.findall(r'rd \d+:\d+', anim)
    get_rd.append(re_to_rd)

new_only_vrf_name = []
vrf_no_name = []
vrf_with_rd = []

name_with_rd = {}

for anim in only_vrf_name:
    re_space_removal = re.match('[^\s]+', anim)
    new_only_vrf_name.append('name: ' + re_space_removal.group(0))
    re_vrf_rd = re.search('\s* (\d+:\d+) \s*', anim)
    if re_vrf_rd != None:
        vrf_with_rd.append(re_vrf_rd.group())
        name_with_rd[ re_space_removal.group(0)] = re_vrf_rd.group()

    
parser_output = []

for anim, bnim in name_with_rd.items():
    text = f" {{ 'name': '{anim}', 'route_distinguisher': '{bnim}'}},"
    parser_output.append(text)

for anim in parser_output:
    print(anim)