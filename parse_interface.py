#!/usr/bin/env python

from os import name
import re
import json

load_from_file = open("router_test_output.txt", "r").read()

new_load = load_from_file.split("!")

interface_name = [anim for anim in new_load if "interface " in anim]

name_of_interface = []
description_fetch = []
interface_status = []
actual_ip = []

filter_interface_name = "\s*interface (.*)\s*"
filter_description = "\s*description (.*)\s*"

for anim in interface_name:

    if "source-interface" not in anim:
        re_interface_name = re.search(filter_interface_name, anim)
        name_of_interface.append(re_interface_name.group(1))
    if "interface" in anim and "source-interface" not in anim:
        re_description = re.search(filter_description, anim)
        if re_description == None:
            description_fetch.append("No Interface Name")
        else:
            description_fetch.append(re_description.group(1))
        if "shutdown" in anim:
            interface_status.append("DOWN")
        else:
            interface_status.append("UP")
        if " ip address " in anim:
            actual_ip.append(
                re.findall(r"(?:[0-9]{1,3}\.){3}[0-9]{1,3}", anim)
            )
        else:
            actual_ip.append([])

interface_name_with_desciption = dict(
    zip(name_of_interface, description_fetch)
)
interface_with_status = dict(zip(name_of_interface, interface_status))
interface_with_ip = dict(zip(name_of_interface, actual_ip))


parser_output = []

for anim, bnim in interface_name_with_desciption.items():
    for cnim, dnim in interface_with_status.items():
        if cnim == anim:
            text = f" {{ 'name': '{anim}',  'desciption': '{bnim}', 'status': '{dnim}' }} "
            parser_output.append(text)
            break

only_interface_with_ip = {}

for anim, bnim in interface_with_ip.items():
    if len(bnim) == 0:
        only_interface_with_ip[anim] = "No IP Address"
    else:
        only_interface_with_ip[anim] = bnim

output_format = str(parser_output).replace('"', '')

print(output_format)
